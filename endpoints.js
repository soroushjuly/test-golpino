// USER
export const CHECK_USER = '/user/check'
export const VERIFY_USER = '/user/verify'
export const RESEND_CODE = '/user/resend/code'
export const ADD_PASSWORD = '/user/add-password'
export const LOGIN = '/login'
