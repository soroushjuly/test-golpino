export const state = () => ({
  user: {},
})

export const mutations = {
  update_user(state, payload) {
    state.user = payload
  },
}
