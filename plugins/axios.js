export default function ({ $axios }) {
  if (process.server) {
    return
  }

  $axios.onRequest((config) => {
    const token = localStorage.getItem('access_token')
    if (token) {
      config.headers.common.Authorization = token
    } else {
      config.headers.common.Authorization = false
    }
  })
}
